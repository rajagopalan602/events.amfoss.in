import React, { useState, useEffect } from 'react';
import './styles/style.sass';

import moment from 'moment';

import Header from './components/header';
import SEO from '../../components/seo';
import Countdown from './components/countdown';
import Registration from './components/registration';
import Footer from './components/footer';
import RSVPForm from './components/rsvpForm';
import LearnMore from './components/learnmore';
import Closed from './components/closed';


const Hacktoberfest = () => {
  var date1 = moment()
    .utcOffset('+05:30')
    .format('DD-MM-YYYY');
  var date2 = '23-11-2022';

  const [hash, setHash] = useState(undefined);
  const [queryLoaded, setQueryLoaded] = useState(false);

  var display;
  if (date1 > date2) {
    display = <Registration />;
  } else {
    display = <Closed />;
  }

  useEffect(() => {
    if (!queryLoaded) {
      const query = window.location.search.substring(1);
      const queryHash = query.split('=');
      setHash(queryHash[1]);
      setQueryLoaded(true);
    }
  });

  return (
    <>
      <SEO title="Hacktoberfest 2022 - Meetup & BootCamp | Amritapuri | October 22nd" />
      {hash === undefined ? (
        <>
          <Header />
          <Countdown deadline="October 22, 2021 7:00 PM" />
          <LearnMore />
          {display}
          <Footer />
        </>
      ) : (
        <RSVPForm hash={hash} />
      )}
    </>
  );
};

export default Hacktoberfest;
