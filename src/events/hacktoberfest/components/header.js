import React from 'react';
import Logo from '../images/Logo-title.png';
import amFOSSLogo from '../../../images/amfoss_logo.png';
import amritaLogo from '../../../images/amrita_logo.png';
import Particles from 'react-particles-js';
import { Link } from 'react-scroll';
import scrollSvg from '../images/scroll.svg'
import { useState } from 'react';
import { useEffect } from 'react';

const Header = () => {

  var s1 = '01001000 01100101 01101100 01101100 01101111 00101100 00100000 01010111 01100101 01101100 01100011 01101111 01101101 01100101 00100000 01110100 01101111 00100000 01100001 01101101 01000110 01001111 01010011 01010011 00100000 01101111 01110000 01100101 01101110 00100000 01110011 01101111 01110101 01110010 01100011 01100101 00100001'

  var s2 = '01001000 01100101 01101100 01101100 01101111 00100000 01010111 01100101 01101100 01100011 01101111 01101101 01100101 00100000 01110100 01101111 00100000 01001000 01100001 01100011 01101011 01110100 01101111 01100010 01100101 01110010 00100000 01100110 01100101 01110011 01110100 00100000 00110010 00110000 00110010 00110010 00100001'

  const [binaryMessage, setBinaryMessage] = useState(s1);

  useEffect(() => {
    const interval = setInterval(() => {
      const d = new Date();
      let seconds = d.getSeconds();
      if (seconds % 2 === 0) {
        setBinaryMessage(s1);
      } else {
        setBinaryMessage(s2);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <div className='binary-message'>
        <p style={{ color: 'gray', fontSize: '0.6rem', textAlign: 'center', zIndex: '100' }}>{binaryMessage}</p>
      </div>
      <br />
      <div id="header-area">
        <Particles
          canvasClassName="particleBg"
          params={{
            particles: {
              number: {
                value: 150,
                density: {
                  enable: true,
                  value_area: 1500
                },
              },
              size: {
                value: 2,
                random: false,
                anim: {
                  speed: 0.5,
                  size_min: 0.5,
                },
              },
              color: {
                value: '#64E3FF',
              },
              line_linked: {
                enable: false,
              },
              move: {
                random: false,
                speed: 0.8,
                direction: 'right',
                out_mode: 'out',
              },
            },
            interactivity: {
              events: {
                onhover: {
                  enable: true,
                  mode: 'push',
                },
              },
              modes: {
                push: {
                  particles_nb: 1
                },
              },
            },
            retina_detect: true
          }}
        />
        <div id="top-bar" className="row mx-4 py-3">
          <div className="col-6">
            <a href="https://amfoss.in">
              <img alt="amFOSS Logo" className="amFOSSLogo" src={amFOSSLogo} />
            </a>
          </div>
          <div className="col-6 text-right">
            <a href="https://www.amrita.edu/">
              <img alt="Amrita Logo" className="amritaLogo" src={amritaLogo} />
            </a>
          </div>
        </div>
        <div className="header-title d-flex align-items-center justify-content-center">
          <div className="m-0 ">
            <div className="text-center py-4">
              <img className="hacktoberLogo" src={Logo} />
            </div>
            <div className='d-flex flex-wrap align-items-center justify-content-between w-70 mx-3'>

              <div style={{ textAlign: 'center' }}>
                <h1>Amritapuri</h1>
                <h3>October 22nd & 23rd</h3>
              </div>
              <div className='mx-auto align-items-center '>
                <Link
                  to="registration-form"
                  smooth={true}
                  duration={1500}
                  style={{ color: 'black', textDecoration: 'none', margin: 'auto' }}
                >
                  <button style={{marginLeft: '3rem'}} className="button" >REGISTER NOW</button>
                </Link>
              </div>


            </div>
            <div id="scroll-down">
              <Link
                to="counter"
                smooth={true}
                duration={1000}
                style={{ color: 'black', textDecoration: 'none' }}
              >
                <div>
                  <img src={scrollSvg} id="scrollbtn"></img>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;