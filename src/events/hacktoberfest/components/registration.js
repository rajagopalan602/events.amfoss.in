import React from 'react';
import dataFetch from '../../../utils/dataFetch';

const query = `
  mutation submitApplication($name: String!, $email: String!, $formData: JSONString!){
  submitApplication(
    name: $name, 
    email: $email,
    formID: 10,
    phone: "0",
    formData: $formData
  )
  {
    id
  }
}
`;
const regquery = `
{
  registrationForm(formID:10)
  {
    applicationsCount
  }
}`;

class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      gender: '',
      roll: '',
      planet: '',
      discord: '',
      errorText: '',
      successText: '',
      loading: false,
      count: 150,
      slotsLeft: 150,
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
    this.handleRollChange = this.handleRollChange.bind(this);
    this.handlePlanetChange = this.handlePlanetChange.bind(this);
    this.handleDiscordChange = this.handleDiscordChange.bind(this);
  }

  handleNameChange(event) {
    this.setState({ name: event.target.value });
  }
  handleEmailChange(event) {
    this.setState({ email: event.target.value.trim() });
  }
  handleGenderChange(event) {
    this.setState({ gender: event.target.value });
  }
  handleRollChange(event) {
    this.setState({ roll: event.target.value.trim() });
  }
  handlePlanetChange(event) {
    this.setState({ planet: event.target.value.trim() });
  }
  handleDiscordChange(event) {
    this.setState({ discord: event.target.value.trim() });
  }

  register = async () => {
    const {
      name,
      email,
      gender,
      roll,
      planet,
      discord,
    } = this.state;
    const emailRegex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (name === '' || email === '' || gender === '') {
      this.setState({
        loading: false,
        errorText: 'Please Fill All the Fields',
      });
    } else if (emailRegex.test(email) === false) {
      this.setState({ loading: false, errorText: 'Enter Proper Email' });
    } else {
      const json = {
        gender: gender,
        rollNo: roll,
        homePlanet: planet,
        discord: discord,
      };
      const variables = { name, email, formData: JSON.stringify(json) };
      const response = await dataFetch({ query, variables });
      console.log(response);
      if (Object.prototype.hasOwnProperty.call(response, 'errors')) {
        this.setState({
          loading: false,
          errorText: response.errors[0].message,
        });
      } else {
        this.setState({
          successText: response.data.submitApplication.id,
          errorText: '',
        });
      }
    }
  };

  getRegisteredCount = async () => {
    const response = await dataFetch({ query: regquery });
    this.setState({
      count: response.data.registrationForm.applicationsCount,
      slotsLeft: 150 - response.data.registrationForm.applicationsCount,
    });
    //this.setState({ count: 0, slotsLeft: 150 - 0 })
  };

  componentDidMount() {
    this.getRegisteredCount();
  }

  render() {
    return (
      <section id="registration-form">
        <div className="row m-0 container mx-auto my-auto">
          <div className=" align-items-start">
            {!this.state.loading ? (
              <div>
                <h2 className="my-4 text-light">
                  {this.state.count > 50 ? (
                    <>
                      <span>{this.state.count}</span> Already Registered.
                      <br />
                    </>
                  ) : (
                    <></>
                  )}
                  {this.state.count < 150 ? (
                    <>
                      <span>
                        {this.state.slotsLeft >= 0 ? this.state.slotsLeft : 0}
                      </span>{' '}
                      Slots Left.
                      <br />
                    </>
                  ) : (
                    <>
                      <span> Join the WaitList</span>
                      <br />
                    </>
                  )}
                  
                  Register <span>Now!</span>
                </h2>
                <p className="text-light">
                  Sign up for the virtual meetup for free by filling up the form
                  below, and make sure you do that fast as we have limited seats
                  to fit you all in! Also, don't forget to bring in your friends
                  as well :)
                </p>
                <br/>
                <p style={{ color: 'red' }}>
                  * Event only open for students registered for amFOSS Tasks.
                </p>
                <form
                  className="form-group"
                  onSubmit={e => {
                    this.setState({ loading: true });
                    this.register();
                    e.preventDefault();
                  }}
                >
                  <div className="row">
                    <div className="col-12 py-2">
                      <div className="m-2">
                        <input
                          type="text"
                          placeholder="Enter Full Name"
                          name="name"
                          className="form-control"
                          onChange={this.handleNameChange}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6 py-2">
                      <div className="m-2">
                        <input
                          type="text"
                          placeholder="Enter Home Civilisation"
                          name="planet"
                          className="form-control"
                          onChange={this.handlePlanetChange}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6 py-2">
                      <div className="m-2">
                        <input
                          type="text"
                          placeholder="Enter amFOSS Discord server name"
                          name="discord"
                          className="form-control"
                          onChange={this.handleDiscordChange}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6 py-2">
                      <div className="m-2">
                        <input
                          type="text"
                          placeholder="Enter Roll Number"
                          name="Roll No"
                          className="form-control"
                          onChange={this.handleRollChange}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6 py-2">
                      <div className="m-2">
                        <select
                          className="form-control text-dark"
                          onChange={this.handleGenderChange}
                        >
                          <option defaultValue="Select Gender" hidden  >
                            Select Gender
                          </option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-12 py-2">
                      <div className="m-2">
                        <input
                          type="text"
                          placeholder="Enter Email"
                          name="email"
                          className="form-control"
                          onChange={this.handleEmailChange}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6 p-0">
                      <div className="m-2" />
                    </div>
                    <div className="col-12 form-check">
                      <div className="m-2 text-dark text-center d-flex justify-content-center">
                        <label
                          className="form-check-label"
                          htmlFor="undertaking"
                        >
                          By submitting this application, I agree to the Code of
                          Conduct of the organizers.
                        </label>
                      </div>
                    </div>
                    {this.state.errorText !== '' ? (
                      <div
                        className="w-100 w-100 text-center m-2 alert alert-danger"
                        role="alert"
                      >
                        {this.state.errorText}
                      </div>
                    ) : null}
                    <div className="mx-auto my-3 p-0 text-center text-md-right">
                      <div className="m-2">
                        <button
                          type="submit"
                          className="button btn-block"
                          id="register-btn"
                          style={{duration: '5000',}}
                        >
                          REGISTER
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            ) : this.state.successText !== '' ? (
              <div className="alert alert-success my-5">
                Thank You! You have successfully signed up for the event. We
                will get back to soon, meanwhile dont forget to bring your
                friends as well!
              </div>
            ) : (
              <div className="alert alert-warning my-5">Submitting. Please Wait</div>
            )}
          </div>
        </div>
      </section>
    );
  }
}
export default Registration;
