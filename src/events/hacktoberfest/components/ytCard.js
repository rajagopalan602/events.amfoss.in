import React from 'react'

function ytCard() {
  return (
    <div id='youtube-card' >
        <div className='outerCard'>
            <div className='content'>
                <iframe src="https://www.youtube.com/embed/TtkP5ZABMjI?autoplay=1&mute=0&loop=1" title="Youtube video player: Hacktoberfest Meetup, Amritapuri campus" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0">
                </iframe>
            </div>
        </div>
    </div>
  )
}

export default ytCard