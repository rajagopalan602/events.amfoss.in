import React from 'react';
import dataFetch from '../../../utils/dataFetch';
import photo from '../images/hacktober-amfoss.jpg';

class Closed extends React.Component {
  render() {
    return (
      <section id="registration-form">
        <div className="row m-0">
          <div
            style={{
              backgroundImage: `url(${photo})`,
              backgroundPosition: 'bottom',
              backgroundRepeat: 'no-repeat',
              objectFit: 'fill',
              backgroundSize: 'cover',
              minHeight: '60vh',
            }}
            className="col-md-7 p-0"
          />
          <div className="col-md-5 p-4 d-flex align-items-center">
            <h2 className="my-4 text-dark">
              Registrations are <span>Closed</span>
            </h2>
          </div>
        </div>
      </section>
    );
  }
}
export default Closed;
