import React from 'react';
import DigitalOcean from '../images/Logo.png';
import YtCard from './ytCard';

const LearnMore = () => {
  return (
    <section id="learn-more">
      <div className="flex-row m-0 d-flex align-items-center">
        <div className="col-3">
          <img src={DigitalOcean} alt="Image" />
        </div>
        <div className="col-8">
          <p className="text-light">
            Hacktoberfest is an annual event hosted by DigitalOcean and GitHub
            promoting and supporting Open Source collaboration. It's all about
            encouraging meaningful contributions to open source.
            <br />
            Join Hacktoberfest meetup hosted by amFOSS on 22nd and 23rd of
            October and <br /> don't miss this oppurtunity to get a{' '}
            <span>Limited edition T-shirt!</span>
          </p>
          <br /><br />
          <button className="button">
            <a
              href="https://hacktoberfest.digitalocean.com/"
              style={{ color: 'black', textDecoration: 'none', duration: '4000' }}
            >
              LEARN MORE!
            </a>
          </button>
        </div>
      </div>
      <YtCard />
    </section>
  );
};

export default LearnMore;
