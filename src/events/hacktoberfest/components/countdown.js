import React, { Component } from 'react';
import '../styles/countdown.css'

class Countdown extends Component {

  constructor(props) {
    super(props);
    this.state = {
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      deadline: 'September, 26, 2022'
    };
  }

  componentWillMount() {
    this.getTimeUntil(this.state.deadline);
  }

  componentDidMount() {
    setInterval(() => this.getTimeUntil(this.state.deadline), 1000);
  }

  leading0(num) {
    return num < 10 ? "0" + num : num;
  }

  getTimeUntil(deadline) {
    const time = Date.parse(deadline) - Date.parse(new Date());
    if (time < 0) {
      this.setState({ days: 0, hours: 0, minutes: 0, seconds: 0 });
    } else {
      const seconds = Math.floor((time / 1000) % 60);
      const minutes = Math.floor((time / 1000 / 60) % 60);
      const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
      const days = Math.floor(time / (1000 * 60 * 60 * 24));
      this.setState({ days, hours, minutes, seconds });
    }
  }

  render() {
    return (
      <>
        <section id="counter" className="d-flex flex-column align-items-center ">
        <h4 className='neonText pulsate ml-2'>{'>>'} TIME TO LAUNCH</h4>
          <div className='countdownBox'>
            <div className='countdownBox-item'>
              <p>days:</p>
              <p>{(this.state.days)}</p>
            </div>
            <div className='countdownBox-item'>
              <p>hours:</p>
              <p>{(this.state.hours)}</p>
            </div>
            <div className='countdownBox-item'>
              <p>minutes:</p>
              <p>{(this.state.minutes)}</p>
            </div>
            <div className='countdownBox-item'>
              <p>seconds:</p>
              <p>{(this.state.seconds)}</p>
            </div>
          </div>
        </section>
      </>
    );
  }
}
export default Countdown;
