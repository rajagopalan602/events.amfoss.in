import React from 'react';
import Digitalocean from '../images/digitalocean.svg';
import Dev from '../images/dev.svg';
import Github from '../images/github.png';
import appwrite from '../images/appwrite.png';
import aviyel from '../images/aviyel.png';
import camunda from '../images/camunda.png';
import medusa from '../images/medusa.png';
import percona from '../images/percona.png';
import shastra from '../images/shastra.png';

const Footer = () => {
  return (
    <>
      <h1 className="py-4 my-4 text-light text-center">#OctoberHasCome</h1>
      <div className="text-center text-light">Thank You</div>
      <div id="footer-logos" className="row m-0">
        <div className="container my-2 d-flex justify-content-center">
          <a href="https://www.digitalocean.com" target='_blank'>
            <img className="px-2 mt-1" src={Digitalocean} alt="Digital Ocean" title='Digital Ocean'/>
          </a>
          <a href="https://github.com/" target='_blank'>
            <img className="px-2 mt-1" src={Github} alt="Github" title='GitHub'/>
          </a>
          {/* <a href="https://dev.to/" target='_blank'>
            <img className="px-2" src={Dev} alt="Dev" />
          </a> */}
          <a href="https://appwrite.io/" target='_blank'>
            <img className="px-2 mt-1" src={appwrite} alt="appwrite" title="Appwrite"/>
          </a>
          <a href="https://aviyel.com/" target='_blank'>
            <img className="px-2 mt-1" src={aviyel} alt="aviyel" title='Aviyel' />
          </a>
          <a href="https://camunda.com/" target='_blank'>
            <img className="mx-1 mt-2" src={camunda} alt="camunda" title='Camunda'/>
          </a>
          <a href="https://medusajs.com/" target='_blank' >
            <img className="px-1 " src={medusa} alt="medusa" title='Medusa'/>
          </a>
          <a href="https://www.percona.com/" target='_blank'>
            <img className="px-2 mt-1" src={percona} alt="percona" title='Percona' />
          </a>
          <a href="https://shastraos.co/" target='_blank'>
            <img className="px-2 mt-1" src={shastra} alt="shastra" title='ShastraOS'/>
          </a>
        </div>
      </div>
    </>
  );
};

export default Footer;
